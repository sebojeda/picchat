package android.ojedas.snapchatproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ChatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ChatFragment chat = new ChatFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, chat). commit();
    }
}
