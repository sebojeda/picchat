package android.ojedas.snapchatproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ContactListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        ContactListFragment contactlist = new ContactListFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, contactlist). commit();
    }
}
