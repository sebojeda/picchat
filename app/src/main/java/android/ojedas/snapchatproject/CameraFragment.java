package android.ojedas.snapchatproject;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.AbstractCollection;
import java.util.Date;

public class CameraFragment extends Fragment {

    public static final int REQUEST_IMAGE_CAPTURE = 1;

    private String imagefilepath;


    public CameraFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera, container, false);

        File imageFile = null;
        try {
            imageFile = createImageFile();
            imagefilepath = imageFile.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (imageFile != null) {
            Intent takePicIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePicIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));

            if (takePicIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(takePicIntent, REQUEST_IMAGE_CAPTURE);
            }
        }

        // Inflate the layout for this fragment
        return view;
    }

    private File createImageFile() throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String filename = "JPEG" + timestamp + "_";

        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(filename, ".jpg", storageDir);
        return image;
    }

    private void addPohtoToGallery(String filepath){
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(filepath);
        Uri uri = Uri.fromFile(f);
        mediaScanIntent.setData(uri);

        getActivity().sendBroadcast(mediaScanIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == Activity.RESULT_OK) {
                addPohtoToGallery(imagefilepath);
            }
        }
    }
}