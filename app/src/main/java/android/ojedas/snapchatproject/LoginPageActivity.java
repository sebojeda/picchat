package android.ojedas.snapchatproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LoginPageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        LoginPageFragment login = new LoginPageFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.Logincontainer, login). commit();
    }
}
