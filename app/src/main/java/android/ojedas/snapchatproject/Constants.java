package android.ojedas.snapchatproject;


public class Constants {
    public static  final String ACTION_ADD_FRIEND = "android.ojedas.snapchatproject.ADD_FRIEND";
    public static  final String ACTION_SEND_FRIEND_REQUEST = "android.ojedas.snapchatproject.SEND_FRIEND_REQUEST";

    public static  final String BROADCAST_ADD_FRIEND_SUCCESS = "android.ojedas.snapchatproject.ADD_FRIEND_SUCCESS";
    public static  final String BROADCAST_ADD_FRIEND_FAILURE = "android.ojedas.snapchatproject.ADD_FRIEND_FAILURE";



}
