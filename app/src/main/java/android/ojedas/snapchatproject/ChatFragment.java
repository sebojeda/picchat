package android.ojedas.snapchatproject;

import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
public class ChatFragment extends Fragment {


    public ChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);
        String[] menuitems = {"Saaaaahhhhh Dude","Hello How r U","Hi bro"};

        ListView listView = (ListView) view.findViewById(R.id.mainMenu);

        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>( getActivity(), android.R.layout.simple_list_item_1, menuitems);
        listView.setAdapter(listViewAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == 0){
                    Intent intent = new Intent(getActivity(), Camera.class);
                    startActivity(intent);
                }else if(position == 1){
                    Toast.makeText(getActivity(), "You clicked the second Item", Toast.LENGTH_SHORT).show();
                }else if(position == 2){Toast.makeText(getActivity(), "You clicked the third Item", Toast.LENGTH_SHORT).show();}

            }
        });


        // Inflate the layout for this fragment
        return view;
    }

}