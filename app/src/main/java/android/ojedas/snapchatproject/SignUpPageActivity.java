package android.ojedas.snapchatproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SignUpPageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_page);

        SignUpPageFragment signup = new SignUpPageFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.SignUpcontainer, signup).commit();
    }
}
