package android.ojedas.snapchatproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID= "14871866-69E5-CD4B-FF00-8FA3A6FE4A00";
    public static final String SECRET_KEY= "57B5A042-02DB-ED12-FF08-8D9515585200";
    public static final String VERSION= "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if(Backendless.UserService.loggedInUser() != ""){
            Log.i("login", "not logged in");
            MainMenuFragment mainmenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainmenu). commit();
        }else{
            Log.i("login", " logged in");
            MenuFragment loggedIn = new MenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, loggedIn).commit();
        }
    }
}
