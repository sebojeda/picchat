package android.ojedas.snapchatproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MessageListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);

        MessageListFragment messagelist = new MessageListFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, messagelist). commit();
    }
}
